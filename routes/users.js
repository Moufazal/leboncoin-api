const router = require('express').Router();

module.exports = (app) => {

    // Create user
    router.post('/create', app.controllers.users.create)

    return router;
};
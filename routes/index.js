module.exports = (app) => {
    //fait appel à la route / dans le fichier login.js
    app.use('/', require('./login')(app));
    //fait appel à la route /users dans le fichier login.js
    app.use('/users', require('./users')(app));
};
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// creation schema pour un nouvel user
var userSchema = new Schema ({
    firstName: String,
    lastName: String,
    birthDate: Date,
    gender: String,
    email: String,
    password: String,
    phone: String,
    isAdmin: {type: Boolean, default: false},
    registrerDate: {type: Date, default: new Date}
})

module.exports = mongoose.model('User', userSchema);
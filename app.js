const express = require('express')
const app = express()

require('./models')(app);
console.log('loading models...');

require('./controllers')(app);
console.log('loading controllers..');

require('./routes')(app);
console.log('loading routes...');


app.listen(3000, function () {
  console.log('Example app listening on port 3000!')
})